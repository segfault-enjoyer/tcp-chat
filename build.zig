const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const ztsl_dep = b.dependency("ztsl", .{
        .target = target,
        .optimize = optimize,
    });

    {
        const tests = b.addTest(.{
            .root_source_file = b.path("src/tests.zig"),
            .target = target,
            .optimize = optimize,
        });
        tests.root_module.addImport("ztsl", ztsl_dep.module("ztsl"));
        const run_tests = b.addRunArtifact(tests);
        const test_step = b.step("test", "Run tests");
        test_step.dependOn(&run_tests.step);

        if (b.args) |args|
            run_tests.addArgs(args);
    }

    {
        const exe = b.addExecutable(.{
            .name = "client",
            .root_source_file = b.path("src/Client.zig"),
            .target = target,
            .optimize = optimize,
        });
        exe.root_module.addImport("ztsl", ztsl_dep.module("ztsl"));

        b.installArtifact(exe);

        const cmd = b.addRunArtifact(exe);
        cmd.step.dependOn(b.getInstallStep());
        const step = b.step("run-client", "Start the client");
        step.dependOn(&cmd.step);

        if (b.args) |args|
            cmd.addArgs(args);
    }

    {
        const exe = b.addExecutable(.{
            .name = "server",
            .root_source_file = b.path("src/Server.zig"),
            .target = target,
            .optimize = optimize,
        });
        exe.root_module.addImport("ztsl", ztsl_dep.module("ztsl"));

        b.installArtifact(exe);

        const cmd = b.addRunArtifact(exe);
        cmd.step.dependOn(b.getInstallStep());
        const step = b.step("run-server", "Start the server");
        step.dependOn(&cmd.step);

        if (b.args) |args|
            cmd.addArgs(args);
    }
}

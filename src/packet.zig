const std = @import("std");

pub const Packet = union(enum) {
    pub const Request = union(enum) {
        pub const Tag = @typeInfo(Request).@"union".tag_type.?;

        pub const help_str =
            \\help           print this message
            \\reg            register user: reg\\user\\password
            \\helo           log in as user: helo\\user
            \\pswrd          after helo use pswrd to identify yourself: pswrd\\password
            \\msg            msg a specific user: msg\\user\\message
            \\msgall         msg all users: msgall\\message
            \\changepswrd    changes password: changepswrd\\new_password
            \\changename     changes name: changename\\new_name
            \\listclient     list all connected users
            \\serverinfo     get info about the server (i.e. ip, ping)
            \\quit           quit the client
            \\
        ;

        pub const strToTag: std.StaticStringMap(Tag) = b: {
            const fields = @typeInfo(Tag).@"enum".fields;
            var kvs: [fields.len]struct { []const u8, Tag } = undefined;
            for (&kvs, fields) |*kv, field|
                kv.* = .{ field.name, @as(Tag, @enumFromInt(field.value)) };
            break :b .initComptime(kvs);
        };

        helo: []const u8,
        pswrd: []const u8,
        changepswrd: struct { old: []const u8, new: []const u8 },
        quit,
        msg: struct { recipient: []const u8, message: []const u8 },
        msgall: []const u8,
        listclient,
        reg: struct { name: []const u8, password: []const u8 },
        changename: []const u8,
        serverinfo: i128,
        help,
    };

    pub const Response = union(enum) {
        success,
        no_account,
        wrong_password,
        not_logged_in,
        not_online,
        name_taken,
        listclient: []const []const u8,
        serverinfo: struct { server_name: []const u8, address: []const u8, timestamp: i128 },
        none,
    };

    message: struct { sender: []const u8, message: []const u8 },
    request: Request,
    response: Response,
};

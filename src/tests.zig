const std = @import("std");

const Client = @import("Client.zig");
const Server = @import("Server.zig");

test {
    const allocator = std.testing.allocator;

    var client: Client = .init(allocator, undefined, undefined, undefined);
    defer client.deinit();

    var server: Server = .init(allocator, undefined);
    defer server.deinit();
}

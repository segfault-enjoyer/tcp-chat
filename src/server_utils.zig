const std = @import("std");

pub fn pollAccept(
    server: *std.net.Server,
    allocator: std.mem.Allocator,
) !?std.net.Server.Connection {
    var poller = std.io.poll(allocator, enum { acc }, .{
        .acc = std.fs.File{ .handle = server.stream.handle },
    });
    defer poller.deinit();

    return if (poller.pollTimeout(0) == error.SocketNotConnected)
        try server.accept()
    else
        null;
}

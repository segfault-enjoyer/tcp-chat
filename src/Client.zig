const std = @import("std");

const ztsl = @import("ztsl");

const Packet = @import("packet.zig").Packet;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var stdout = std.io.getStdOut();
    defer stdout.close();

    var stdin = std.io.getStdIn();
    defer stdin.close();

    var args = std.process.args();
    _ = args.next();
    const stream = try std.net.tcpConnectToHost(allocator, args.next() orelse "::1", 4000);
    defer stream.close();

    std.log.info("connected successfully", .{});

    var client: Client = .init(allocator, stdout.writer(), stdin.reader(), stream);
    defer client.deinit();
    client.start() catch |e| switch (e) {
        error.EndOfStream => try stdout.writer().print("quit client\n", .{}),
        else => return e,
    };
}

const Client = @This();

allocator: std.mem.Allocator,

out: std.fs.File.Writer,
in: std.fs.File.Reader,
server: std.net.Stream,
pollfds: [2]std.posix.pollfd,

pub fn init(
    allocator: std.mem.Allocator,
    out: std.fs.File.Writer,
    in: std.fs.File.Reader,
    server: std.net.Stream,
) Client {
    return .{
        .allocator = allocator,
        .server = server,
        .out = out,
        .in = in,
        .pollfds = .{
            .{ .fd = in.context.handle, .events = std.posix.POLL.IN, .revents = undefined },
            .{ .fd = server.handle, .events = std.posix.POLL.IN, .revents = undefined },
        },
    };
}

pub fn deinit(self: *Client) void {
    self.* = undefined;
}

pub fn start(
    self: *Client,
) !void {
    var arena_impl: std.heap.ArenaAllocator = .init(self.allocator);
    defer arena_impl.deinit();
    const arena = arena_impl.allocator();

    while (true) {
        defer _ = arena_impl.reset(.{ .retain_with_limit = 4 * 1024 });

        _ = try std.posix.poll(&self.pollfds, -1);

        if (self.pollfds[0].revents & std.posix.POLL.IN > 0) b: {
            const input = try self.in.readUntilDelimiterAlloc(arena, '\n', std.math.maxInt(usize));
            const request = parse(input) catch |e| {
                try self.out.print("ERROR: {s}\n", .{@errorName(e)});
                break :b;
            };
            if (request == .help) {
                try self.out.writeAll(Packet.Request.help_str);
                break :b;
            }
            try self.sendRequest(request);
        }

        if (self.pollfds[1].revents & std.posix.POLL.IN > 0) {
            const packet = try ztsl.deserialize(Packet, arena, self.server.reader());
            try self.handlePacket(packet);
        }
    }
}

fn sendRequest(self: *Client, request: Packet.Request) !void {
    const packet: Packet = .{ .request = request };
    var bufwriter = std.io.bufferedWriter(self.server.writer());
    try ztsl.serialize(bufwriter.writer(), packet);
    try bufwriter.flush();
}

fn handlePacket(self: *Client, packet: Packet) !void {
    switch (packet) {
        .message => |m| try self.out.print("{s}: {s}\n", .{ m.sender, m.message }),
        .request => unreachable,
        .response => |r| switch (r) {
            .success => try self.out.writeAll("success\n"),
            .no_account => try self.out.writeAll("no account provided\n"),
            .wrong_password => try self.out.writeAll("wrong_password\n"),
            .not_online => try self.out.writeAll("recipient not online\n"),
            .name_taken => try self.out.writeAll("name is taken\n"),
            .not_logged_in => try self.out.writeAll("not logged in\n"),
            .listclient => |clients| for (clients) |client|
                try self.out.print("{s}\n", .{client}),
            .serverinfo => |info| try self.out.print(
                \\{s} at {s}
                \\ping: {}
                \\
            , .{
                info.server_name,
                info.address,
                std.fmt.fmtDurationSigned(@intCast(std.time.nanoTimestamp() - info.timestamp)),
            }),
            .none => unreachable,
        },
    }
}

fn parse(in: []const u8) !Packet.Request {
    const delimiter = "\\\\";

    var iter = std.mem.splitSequence(u8, std.mem.trimRight(u8, in, "\r\n"), delimiter);

    const tag = Packet.Request.strToTag.get(iter.next() orelse return error.InvalidInput) orelse
        return error.InvalidInput;
    const request: Packet.Request = switch (tag) {
        .quit => .quit,
        .help => .help,
        .helo => .{ .helo = iter.next() orelse return error.InvalidInput },
        .pswrd => .{ .pswrd = iter.next() orelse return error.InvalidInput },
        .msgall => .{ .msgall = iter.next() orelse return error.InvalidInput },
        .changename => .{ .changename = iter.next() orelse return error.InvalidInput },
        .serverinfo => .{ .serverinfo = std.time.nanoTimestamp() },
        .changepswrd => .{ .changepswrd = .{
            .old = iter.next() orelse return error.InvalidInput,
            .new = iter.next() orelse return error.InvalidInput,
        } },
        .msg => .{ .msg = .{
            .recipient = iter.next() orelse return error.InvalidInput,
            .message = iter.next() orelse return error.InvalidInput,
        } },
        .listclient => .listclient,
        .reg => .{ .reg = .{
            .name = iter.next() orelse return error.InvalidInput,
            .password = iter.next() orelse return error.InvalidInput,
        } },
    };

    if (iter.next()) |_|
        return error.InvalidInput;

    return request;
}

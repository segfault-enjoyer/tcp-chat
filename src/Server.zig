const std = @import("std");

const ztsl = @import("ztsl");

const Packet = @import("packet.zig").Packet;
const server_utils = @import("server_utils.zig");

pub fn main() !void {
    var alloc_impl: std.heap.GeneralPurposeAllocator(.{}) = .init;
    defer _ = alloc_impl.deinit();
    const allocator = alloc_impl.allocator();

    var args = std.process.args();
    _ = args.next();
    const address = try std.net.Address.parseIp(args.next() orelse "::1", 4000);
    var tcp_server = while (true) {
        if (address.listen(.{})) |addr|
            break addr
        else |err| switch (err) {
            error.AddressInUse => {
                const delay = std.time.ns_per_s * 32;
                std.log.err(
                    "{s}, retrying in {}",
                    .{ @errorName(err), std.fmt.fmtDuration(delay) },
                );
                std.time.sleep(delay);
            },
            else => return err,
        }
    };
    defer tcp_server.deinit();
    std.log.info("listening on {}", .{tcp_server.listen_address});

    var server: Server = .init(allocator, tcp_server);
    defer server.deinit();
    try server.start();
}

const Server = @This();

allocator: std.mem.Allocator,
server: std.net.Server,
chat: ChatData,

pub fn init(allocator: std.mem.Allocator, server: std.net.Server) Server {
    return .{
        .allocator = allocator,
        .server = server,
        .chat = .{ .allocator = allocator },
    };
}

pub fn deinit(self: *Server) void {
    self.chat.deinit();
    self.* = undefined;
}

pub fn start(self: *Server) !void {
    var arena_impl: std.heap.ArenaAllocator = .init(self.allocator);
    defer arena_impl.deinit();
    const arena = arena_impl.allocator();

    while (true) {
        defer _ = arena_impl.reset(.{ .retain_with_limit = 16 * 1024 });

        if (try server_utils.pollAccept(&self.server, arena)) |c| {
            try self.chat.clients.append(self.chat.allocator, .{ .connection = c });
            std.log.info("{} connected", .{c.address});
        }

        if (self.chat.clients.items.len == 0) {
            std.time.sleep(std.time.ns_per_ms * 100);
            continue;
        }

        const pfds = try arena.alloc(std.posix.pollfd, self.chat.clients.items.len);
        for (pfds, self.chat.clients.items) |*pfd, client|
            pfd.* = .{
                .fd = client.connection.stream.handle,
                .events = std.posix.POLL.IN,
                .revents = undefined,
            };

        const new = try std.posix.poll(pfds, 100);
        if (new == 0)
            continue;

        try self.handleInput(arena, pfds);
    }
}

pub fn handleInput(
    self: *Server,
    arena: std.mem.Allocator,
    pfds: []std.posix.pollfd,
) !void {
    var fds: std.ArrayListUnmanaged(std.posix.pollfd) = .fromOwnedSlice(pfds);

    var i: usize = 0;
    while (i < self.chat.clients.items.len) {
        if (fds.items[i].revents == 0) {
            i += 1;
            continue;
        }

        const client_connected = try self.handlePacket(
            arena,
            &self.chat.clients.items[i],
        );

        if (!client_connected) {
            self.chat.clients.swapRemove(i).connection.stream.close();
            _ = fds.swapRemove(i);
            continue;
        }

        i += 1;
    }
}

pub fn handlePacket(self: *Server, arena: std.mem.Allocator, client: *Client) !bool {
    const stream = &client.connection.stream;

    const packet = ztsl.deserialize(Packet, arena, stream.reader()) catch |e| switch (e) {
        error.EndOfStream => {
            std.log.info("{} disconnected", .{client.connection.address});
            return false;
        },
        else => {
            std.log.warn("{} sent broken packet: {s}", .{ client.connection.address, @errorName(e) });
            return true;
        },
    };

    if (packet != .request) {
        std.log.warn(
            "{} sent wrong packet: {s}",
            .{ client.connection.address, @tagName(std.meta.activeTag(packet)) },
        );
        return true;
    }

    try self.chat.process(&self.server, client, packet.request);
    return true;
}

const ChatData = struct {
    allocator: std.mem.Allocator,
    clients: std.ArrayListUnmanaged(Client) = .{},
    accounts: std.ArrayListUnmanaged(Account) = .{},

    fn process(
        self: *ChatData,
        server: *std.net.Server,
        client: *Client,
        req: Packet.Request,
    ) !void {
        var arena_impl: std.heap.ArenaAllocator = .init(self.allocator);
        arena_impl.deinit();
        const arena = arena_impl.allocator();

        const response = switch (req) {
            .helo => |name| try self.helo(client, name),
            .pswrd => |password| try self.pswrd(client, password),
            .changepswrd => |password| try self.changepswrd(client, password.old, password.new),
            .quit => try self.quit(client),
            .msg => |message| try self.msg(client, message.recipient, message.message),
            .msgall => |message| try self.msgall(client, message),
            .listclient => try self.listclient(arena, client),
            .reg => |acc| try self.reg(client, acc.name, acc.password),
            .changename => |new_name| try self.changename(client, new_name),
            .serverinfo => |time| try self.serverinfo(arena, server, time),
            .help => {
                std.log.warn(
                    "{} sent the clientside only request {s}",
                    .{ client.connection.address, @tagName(req) },
                );
                return;
            },
        };

        std.log.info(
            "ip={} request={s} response={s}",
            .{ client.connection.address, @tagName(req), @tagName(response) },
        );

        if (response == .none)
            return;

        var bufwriter = std.io.bufferedWriter(client.connection.stream.writer());
        const packet: Packet = .{ .response = response };
        try ztsl.serialize(bufwriter.writer(), packet);
        try bufwriter.flush();
    }

    fn helo(self: *ChatData, client: *Client, name: []const u8) !Packet.Response {
        client.account = .{ .id = self.indexOfAccount(name) orelse return .no_account };
        return .success;
    }

    fn pswrd(self: *ChatData, client: *Client, password: []const u8) !Packet.Response {
        const acc_info = client.account orelse
            return .no_account;

        const acc = self.accounts.items[acc_info.id];
        if (!std.mem.eql(u8, acc.password, password))
            return .wrong_password;

        client.account.?.logged_in = true;
        return .success;
    }

    fn changepswrd(
        self: *ChatData,
        client: *Client,
        old: []const u8,
        new: []const u8,
    ) !Packet.Response {
        const acc_info = client.account orelse
            return .not_logged_in;
        if (!acc_info.logged_in)
            return .not_logged_in;

        const acc = &self.accounts.items[acc_info.id];
        if (!std.mem.eql(u8, acc.password, old))
            return .wrong_password;

        acc.password = new;
        return .success;
    }

    fn quit(self: *ChatData, client: *Client) !Packet.Response {
        for (self.clients.items, 0..) |*c, i| {
            if (c == client) {
                self.clients.swapRemove(i).connection.stream.close();
                return .none;
            }
        }
        unreachable;
    }

    fn msg(
        self: *ChatData,
        client: *Client,
        name: []const u8,
        message: []const u8,
    ) !Packet.Response {
        const acc_info = client.account orelse
            return .not_logged_in;
        if (!acc_info.logged_in)
            return .not_logged_in;

        const sender = self.accounts.items[acc_info.id];

        const cli_idx = self.indexOfClient(name) orelse
            return .not_online;

        const packet: Packet = .{ .message = .{ .sender = sender.name, .message = message } };
        var bufwriter = std.io.bufferedWriter(self.clients.items[cli_idx].connection.stream.writer());
        try ztsl.serialize(bufwriter.writer(), packet);
        try bufwriter.flush();

        return .success;
    }

    fn msgall(
        self: *ChatData,
        client: *Client,
        message: []const u8,
    ) !Packet.Response {
        const acc_info = client.account orelse
            return .not_logged_in;
        if (!acc_info.logged_in)
            return .not_logged_in;

        const sender = self.accounts.items[acc_info.id];
        const packet: Packet = .{ .message = .{ .sender = sender.name, .message = message } };

        for (self.clients.items) |c| {
            if (c.account == null or c.account.?.id == acc_info.id)
                continue;
            var bufwriter = std.io.bufferedWriter(c.connection.stream.writer());
            try ztsl.serialize(bufwriter.writer(), packet);
            try bufwriter.flush();
        }

        return .success;
    }

    fn listclient(self: *ChatData, arena: std.mem.Allocator, client: *Client) !Packet.Response {
        const acc_info = client.account orelse
            return .not_logged_in;
        if (!acc_info.logged_in)
            return .not_logged_in;

        var list: std.ArrayList([]const u8) =
            try .initCapacity(arena, self.clients.items.len);
        for (self.clients.items) |c| {
            const acc = c.account orelse
                continue;
            if (!acc.logged_in)
                continue;
            list.appendAssumeCapacity(self.accounts.items[acc.id].name);
        }

        return .{ .listclient = list.items };
    }

    fn reg(
        self: *ChatData,
        client: *Client,
        name: []const u8,
        password: []const u8,
    ) !Packet.Response {
        if (self.indexOfAccount(name) != null)
            return .name_taken;

        try self.accounts.append(self.allocator, .{
            .name = try self.allocator.dupe(u8, name),
            .password = try self.allocator.dupe(u8, password),
        });
        client.account = .{
            .id = self.accounts.items.len - 1,
            .logged_in = true,
        };
        return .success;
    }

    fn changename(
        self: *ChatData,
        client: *Client,
        new: []const u8,
    ) !Packet.Response {
        const acc_info = client.account orelse
            return .not_logged_in;
        if (!acc_info.logged_in)
            return .not_logged_in;

        const acc = &self.accounts.items[acc_info.id];

        const old = acc.name;
        defer self.allocator.free(old);

        acc.name = try self.allocator.dupe(u8, new);

        return .success;
    }

    fn serverinfo(
        _: *ChatData,
        arena: std.mem.Allocator,
        server: *std.net.Server,
        time: i128,
    ) !Packet.Response {
        return .{ .serverinfo = .{
            .server_name = "tcp-chat",
            .address = try std.fmt.allocPrint(arena, "{}", .{server.listen_address}),
            .timestamp = time,
        } };
    }

    fn indexOfAccount(self: ChatData, name: []const u8) ?usize {
        for (self.accounts.items, 0..) |acc, i|
            if (std.mem.eql(u8, acc.name, name))
                return i;
        return null;
    }

    fn indexOfClient(self: ChatData, name: []const u8) ?usize {
        for (self.clients.items, 0..) |cli, i|
            if (cli.account) |acc|
                if (std.mem.eql(u8, self.accounts.items[acc.id].name, name))
                    return i;
        return null;
    }

    pub fn deinit(self: *ChatData) void {
        self.clients.deinit(self.allocator);
    }
};

const Client = struct {
    connection: std.net.Server.Connection,
    account: ?struct {
        id: usize,
        logged_in: bool = false,
    } = null,
};

const Account = struct {
    name: []const u8,
    password: []const u8,
};
